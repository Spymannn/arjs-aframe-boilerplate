# arjs-aframe-boilerplate
Initial project with AFrame and AR.js dependancies

## Getting Started


### Prerequisites

You'll need npm 

### Installing

Clone the repo

```
git clone git@gitlab.com:Spymannn/arjs-aframe-boilerplate.git
```
Enter to the directory 

```
cd arjs-aframe-boilerplate
```

Install dependancies

```
npm install
```

Run the application

```
npm start
```

Check your localhost:3000

## Built With

* [AFrame](https://aframe.io/) - AFrame     
* [AR.js](https://github.com/jeromeetienne/AR.js/blob/master/README.md) - AR.js   

## Authors

* **Gitlab account** - [Spymannn](https://gitlab.com/Spymannn)
* **Github account** - [Spymannn](https://github.com/Spymannn)
* **Facebook** - [Spymannn15](https://www.facebook.com/Spyman15/)
* **Twitter** - [Spymannn](https://twitter.com/Spymannn)


